﻿#define _CRT_SECURE_NO_WARNINGS     //отключение ошибки компилятора
#include <iostream>
#include <ctime>

using std::cout;
using std::endl;

int main()
{
    //создание массива
    const int N = 6;    //размерность массива
    int array[N][N];

    //заполнение массива и вывод
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {              
            array[i][j] = i + j;

            cout << array[i][j];
        }
        cout << endl;
    }

    cout << endl;

    //получение текущего числа месяца
    time_t seconds = time(NULL);
    tm* timeinfo = localtime(&seconds);
    int day = timeinfo->tm_mday;
    int index = day % N;    //получение целочисленного остатка от деления

    int sum = 0;

    //подсчёт суммы ряда
    for (int i = 0; i < N; i++) 
    {
        sum = sum + array[index][i];
    }

    cout << sum << endl;
}